/*
* PIN-code checking
* */

 public class PinCodeCheckWindow extends Window {

     private Atm atm;
     private String tempPin;

     public PinCodeCheckWindow(Atm atm){
         this.atm = atm;
         tempPin = atm.getUser().getAccount().getPin();
     }

     @Override
     public void perform(){
         System.out.println("Please, input your PIN-code (hint: " + tempPin + ")");

         String input = Reader.inputString();
         if(input.equals(tempPin)){
             doIfYes();
         } else {
             doIfNo();
         }

     }

     public void doIfYes(){
         atm.setCurrentWindow(new MenuWindow(atm));
     }

     public void doIfNo(){

         int counter = 3; // Count of attempts. After 3 wrong inputs card will be blocked
         while(counter>0){
             int temp = --counter;
             if (counter == 0) { //count of attempts checking
                 System.out.println("Your card has blocked! Please, call to your bank");
                 System.exit(0);
             }
             System.out.println("Wrong PIN-code. You have another "+ temp +
                     " attempt(s). Please, try again, or press \"no\" to exit"); //another attempt
             String input = Reader.inputString();
             if(input.toLowerCase().equals("no")){ // if "no" -> exit
                 System.out.println("Have a nice day!");
                 System.exit(0);
             } else if(input.equals(tempPin)){
                 doIfYes(); // if "yes" -> go to the main menu
                 break;
             } else {
                 continue;
             }
         }
     }
}
