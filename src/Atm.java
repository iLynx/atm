/*
* Describes the main ATM's logic
* */

public class Atm {

    private User currentUser;
    private Window currentWindow;

    public Atm(User user){
        this.currentUser = user;
        currentWindow = new StartWindow(this);
    }

    public Window getCurrentWindow(){
        return currentWindow;
    }

    public void setCurrentWindow(Window window){
        currentWindow = window;
    }

    public void work(){
        while(true){
            currentWindow.perform();
        }
    }

    public User getUser(){
        return currentUser;
    }
}
