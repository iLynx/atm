/*
* Abstract class that describes ATM's window during the work
* */
public abstract class Window {

    // Describes what to do (main logic)
    public abstract void perform();

}
