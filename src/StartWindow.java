/*
* Describes ATM's start window with card inserting and PIN-code checking
* */



public class StartWindow extends Window {
    private Atm atm;

    public StartWindow(Atm atm){
        this.atm = atm;
    }

    @Override
    public void perform() {
        System.out.println("Please, insert your credit card (y/n)");

        String answer = Reader.inputString();
        if(answer.toLowerCase().equals("y")){
            atm.setCurrentWindow(new PinCodeCheckWindow(atm));
        } else {
            System.out.println("Have a nice day!");
            System.exit(0);
        }

    }
}
