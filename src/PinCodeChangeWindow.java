/*
* Changes current PIN-code
* */


public class PinCodeChangeWindow extends Window{

    private Atm atm;
    private String currentPin;

    public PinCodeChangeWindow(Atm atm){
        this.atm = atm;
        currentPin = atm.getUser().getAccount().getPin();
    }

    @Override
    public void perform(){
        System.out.println("Please, enter your current PIN-code (hint: " + currentPin + ")");
        String userInput = Reader.inputString();
        if(userInput.equals(currentPin)){
            pinCodeChanging();
        } else {
            pinCodeChecking();
        }

    }

    public void pinCodeChecking(){
        int counter = 3; // Count of attempts. After 3 wrong inputs card will be blocked
        while(counter>0){
            int temp = --counter;
            if (counter == 0) { //count of attempts checking
                System.out.println("Your card has blocked! Please, call to your bank");
                System.exit(0);
            }
            System.out.println("Wrong PIN-code. You have another "+ temp +
                    " attempt(s). Please, try again, or press \"no\" to exit"); //another attempt
            String input = Reader.inputString();
            if(input.toLowerCase().equals("no")){ // if "no" -> exit
                System.out.println("Have a nice day!");
                System.exit(0);
            } else if(input.equals(currentPin)){
                pinCodeChanging();
                break;
            } else {
                continue;
            }
        }
    }

    public void pinCodeChanging(){
        System.out.println("Please, add new PIN-code");

        while(true){
            try{
                String input = Reader.inputString();
                if(input.length() != 4) {
                    System.out.println("Wrong PIN-code format. Please, try again");
                    continue;
                }
                int newPin = Integer.parseInt(input);
                atm.getUser().getAccount().setPin(input);
                break;
            }catch(NumberFormatException e){
                System.out.println("Wrong PIN-code format. Please, try again");
                continue;
            }
        }
        System.out.println("PIN-code has been changed successfully");
        choice();
    }

    public void choice(){
        System.out.println("Do you want to proceed (y/n)");
        String input = Reader.inputString();
        if(input.toLowerCase().equals("y")){
            atm.setCurrentWindow(new MenuWindow(atm));
        } else {
            System.out.println("Have a nice day!");
            System.exit(0);
        }
    }
}
