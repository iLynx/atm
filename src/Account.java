/*
* describes User's account
*
* */

import java.util.ArrayList;

public class Account {

    private User owner; //owner of account
    private int invoice; // count of money
    private String pin; // PIN-code
    private ArrayList<String> history = new ArrayList<>(); //shows history of banking manipulations (push or take the cash)

    public Account(User owner, int invoice){
        this.owner = owner;
        this.invoice = invoice;
        this.pin = generatePin();
    }

    private String generatePin(){ // generating of random PIN-code for the User
        String result = "";
        for (int i=0; i<4; i++){
            result += (int) (Math.random()*10);
        }
        return result;
    }

    public User getOwner(){
        return owner;
    }

    public int getInvoice(){
        return invoice;
    }

    public void setInvoice(int value){
        this.invoice = value;
    }

    public void setPin(String pin){
        this.pin = pin;
    }

    public String getPin(){
        return pin;
    }

    public ArrayList<String> getHistory(){
        return history;
    }


}
