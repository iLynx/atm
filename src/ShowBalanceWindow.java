/*
* Shows the balance of current account
* */


public class ShowBalanceWindow extends Window{

    private Atm atm;

    public ShowBalanceWindow(Atm atm){
        this.atm = atm;
    }

    @Override
    public void perform(){
        System.out.println("Current owner: " + atm.getUser().getName());
        System.out.println("Total: " + atm.getUser().getAccount().getInvoice() + "$");
        System.out.println("--------");
        choice();
    }

    public void choice(){
        System.out.println("Do you want to proceed (y/n)");
        String input = Reader.inputString();
        if(input.toLowerCase().equals("y")){
            atm.setCurrentWindow(new MenuWindow(atm));
        } else {
            System.out.println("Have a nice day!");
            System.exit(0);
        }
    }
}
