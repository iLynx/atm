/*
* Shows account's history
* */

import java.util.ArrayList;

public class HistoryViewerWindow extends Window{

    private Atm atm;
    private Account currentAccount;

    public HistoryViewerWindow(Atm atm){
        this.atm = atm;
        this.currentAccount = atm.getUser().getAccount();
    }

    @Override
    public void perform(){
        ArrayList<String> list = currentAccount.getHistory();
        System.out.println();
        System.out.println("Current owner: " + atm.getUser().getName());
        System.out.println("============================");
        System.out.println("List of your last operations");
        System.out.println("----------------------------");
        for(String s: list){
            System.out.println(s);
        }
        System.out.println("----------------------------");
        System.out.println("Total: " + currentAccount.getInvoice() + "$");
        System.out.println("============================");

        choice();
    }

    public void choice(){
        System.out.println("Do you want to proceed (y/n)");
        String input = Reader.inputString();
        if(input.toLowerCase().equals("y")){
            atm.setCurrentWindow(new MenuWindow(atm));
        } else {
            System.out.println("Have a nice day!");
            System.exit(0);
        }
    }
}
