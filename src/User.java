/*
* This class describes User's main characteristics
* */


public class User {

    private String firstName;
    private String lastName;
    private Account account;

    public User(String firstName, String lastName){
        this.firstName = firstName;
        this.lastName = lastName;
        this.account = new Account(this, 4520); // just for checking
    }

    @Override
    public String toString(){
        return firstName + " " + lastName;
    }

    public String getName(){
        return this.toString();
    }

    public Account getAccount(){
        return account;
    }
}
