/*
* Allows to put money into your account
* */

 public class PutTheCashWindow extends Window{

     private Atm atm;
     private Account currentAccount;

     public PutTheCashWindow(Atm atm){
         this.atm = atm;
         currentAccount = atm.getUser().getAccount();
     }

     @Override
     public void perform(){
         System.out.println("Please, insert the sum that you want to add to your account");
         boolean clause = true;
         while(clause){
             try{
                 int sum = Integer.parseInt(Reader.inputString());
                 currentAccount.getHistory().add("Putted " + sum + "$");
                 currentAccount.setInvoice(currentAccount.getInvoice() + sum);
                 billPrinting(sum);
                 clause = false;
             }catch(NumberFormatException e){
                 System.out.println("Wrong input! Please, try again");
                 continue;
             }
         }
         choice();

     }

     public void billPrinting(int sum){
         //some code
         System.out.println("+------------------+");
         System.out.println("|       Bill       |");
         System.out.println("+------------------+");
         System.out.println("| Putted " + sum + "$");
         System.out.println("| Total: " + currentAccount.getInvoice() + "$");
         System.out.println("+------------------+");
     }

     public void choice(){
         System.out.println("Do you want to proceed (y/n)");
         String input = Reader.inputString();
         if(input.toLowerCase().equals("y")){
             atm.setCurrentWindow(new MenuWindow(atm));
         } else {
             System.out.println("Have a nice day!");
             System.exit(0);
         }
     }
 }
