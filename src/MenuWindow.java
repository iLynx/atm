/*
* It used for showing the main menu of cash machine
* */


public class MenuWindow extends Window {

    private Atm atm;

    public MenuWindow(Atm atm){
        this.atm = atm;
    }

    @Override
    public void perform(){
        //menu (switch)
        int userChoice = -1;
        while(userChoice < 0) {
            System.out.println("Please, switch the point of menu, or press \"0\" to exit");
            System.out.println("1. Take the cash");
            System.out.println("2. Put the cash");
            System.out.println("3. Show the balance");
            System.out.println("4. Change PIN-code");
            System.out.println("5. Account history");

            try {
                userChoice = Integer.parseInt(Reader.inputString());
            } catch (NumberFormatException e) {
                System.out.println("Wrong input! Please, try again");
                userChoice = -1;
            }
        }

        switch (userChoice) {
            case (1): { //taking the cash
                atm.setCurrentWindow(new TakeTheCashWindow(atm));
                break;
            }
            case (2): { //putting the cash
                atm.setCurrentWindow(new PutTheCashWindow(atm));
                break;
            }
            case (3): { //balance showing
                atm.setCurrentWindow(new ShowBalanceWindow(atm));
                break;
            }
            case (4): { //PIN-code changing
                atm.setCurrentWindow(new PinCodeChangeWindow(atm));
                break;
            }
            case (5): { //history viewing
                atm.setCurrentWindow(new HistoryViewerWindow(atm));
                break;
            }
            default: {
                System.out.println("Have a nice day!");
                System.exit(0);
            }
        }

    }
}
