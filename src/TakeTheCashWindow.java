/*
* This window shows the menu for taking the cash from your invoice
* */



public class TakeTheCashWindow extends Window {

    private Atm atm;
    private Account currentAccount;

    public TakeTheCashWindow(Atm atm){
        this.atm = atm;
        currentAccount = atm.getUser().getAccount();
    }

    @Override
    public void perform(){
        System.out.println("Please, insert the sum that you want to to draw from your account");
        boolean clause = true;
        while(clause){
            try{
                int sum = Integer.parseInt(Reader.inputString());
                drawingMoney(sum);
                //add the check for sum%nominal
                clause = false;
            }catch(NumberFormatException e){
                System.out.println("Wrong input, please, try again!");
                continue;
            }
        }

    }

    public void drawingMoney(int sum){
        boolean clause = true;
        if((currentAccount.getInvoice() - sum) < 0) {
            System.out.println("Out of money");
            clause = false;
        }
        if(clause) {
            currentAccount.setInvoice(currentAccount.getInvoice()-sum);
            currentAccount.getHistory().add("Drawing of " + sum + "$");
            billPrinting(sum);
        }
        choice();

    }

    public void billPrinting(int sum){
        //some code
        System.out.println("+------------------+");
        System.out.println("|       Bill       |");
        System.out.println("+------------------+");
        System.out.println("| Drawing of " + sum + "$");
        System.out.println("| Total: " + currentAccount.getInvoice() + "$");
        System.out.println("+------------------+");
    }

    public void choice(){
        System.out.println("Do you want to proceed (y/n)");
        String input = Reader.inputString();
        if(input.toLowerCase().equals("y")){
            atm.setCurrentWindow(new MenuWindow(atm));
        } else {
            System.out.println("Have a nice day!");
            System.exit(0);
        }
    }

}
