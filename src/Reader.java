/*
* Read user's input from keyboard
* */


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Reader {

    static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public static String inputString(){
        String input = "";
        try{
            input = reader.readLine();
        }catch (IOException e){
            e.printStackTrace();
        }
        return input;
    }
}
